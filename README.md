### Video
https://www.youtube.com/watch?v=JBzoDiJ2TJw

# FusionOrderDemo_R2_1_revised

### Store Front Demo Requirements
The application has the following basic requirements:

An Oracle database (any modern edition) is required for the sample schema
JDeveloper 11.1.2.1 Production, aka R2 (the application will not run in previous versions of JDeveloper). Previous versions of the demo are not compatible with the 11.1.2.1 version of JDeveloper - see the bottom of the page for links to older versions of the demo.
Screen resolution of 1024 x 768 or higher

### Installing the Sample Schema
 

1 Extract FusionOrderDemo_R2.zip to your hard drive

2 In JDeveloper 11.1.2.1, choose File > Open from the main menu

3 Browse to the location where you extracted the zip in Step 1 and select Infrastucture.jws from the Infrastructure directory. Click Open

4 Expand the MasterBuildScript project and Resources node

5 Double click build.properties to open it in the editor

6 Modify the following properties for your environment:

7 

|    |    | 
|----------|:-------------:|
| jdeveloper.home | The /jdeveloper directory where you have JDeveloper 11 installed. For example C:/JDeveloper_11/jdeveloper/ |
| jdbc.urlBase | The base jdbc url for your database in the format jdbc:oracle:thin:@<yourhostname>. 
For example, 
jdbc:oracle:thin:@localhost |
| jdbc.port | The port for your database. For example, 1521 |
| jdbc.sid | The SID of your database. For example, 
ORCL or XE  |
| db.adminUser | The administrative user for your database. For example, system  |	 
| db.demoUser.tablespace | The tablespace name where the FOD user will be installed. For example, USERS  |	 
	 
	
Keep all other properties set to their default values. The demo user must be named FOD.

8 Choose File > Save All from the main menu

9 In the application navigator, under the Resources node, Right click build.xml and choose Run Ant Target > buildAll

10 Enter the password for the system account on your database

The ant script creates the FOD user and populates the tables in the FOD schema. In the Apache Ant - Log, you will see a series of sql scripts and finally: 

buildAll:

BUILD SUCCESSFUL 
Total time: nn minutes nn seconds



### Running the Sample

To run the sample in JDeveloper:

1 Choose File > Open from the main menu

2 Browse to the location where you extracted the demo zip and select StoreFrontModule.jws from the StoreFrontModule directory. Click Open

3 In the Application Navigator, expand the Application Resources accordion. Expand the Connections and Database nodes. Right click FOD and choose Properties

4 Modify only the following properties so that they point to the database where you installed the FOD schema: 

|    |    | 
|----------|:-------------:|
| Host Name | The hostname for your database. For example, localhost |
| JDBC Port | The port for your database. For example, 1521 |
| SID | The SID for your database. For example, ORCL or XE |


The connection must be named FOD (case sensitive) and the username and password must be fod/fusion. Click OK.

5 In the Application Navigator, right click StoreFrontService and choose Rebuild

6 Right click StoreFrontUI and choose Run. The home.jspx page within the StoreFrontUI project is the default run target and will launch the main application screen

7 Note that several other examples are included in the demo. View the readme.txt file in the root directory of the demo for more information about the workspaces included in the demo


### Deploying the Sample

There are several ways to deploy the sample. If you are an ADF developer are are looking for ADF business component, task flow, and ADF Faces rich client examples, the easiest way to deploy the sample is via the "right-click, run" method described above. This will configure the integrated WLS server will all the relevant components required for the StoreFront demo.
