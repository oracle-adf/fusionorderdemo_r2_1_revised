About the Oracle Fusion Order Demo Application

Fusion Order Demo (FOD) is an end-to-end application sample developed by Fusion Middleware Product Management. The
purpose of the demo is to demostrate common use cases in Fusion Middleware applications, including the integration
between different components of the Fusion technology stack. The demo contains several
applications that make up various parts of functionality.

About the Applications included in the Demo (organized by extracted directory name)

Infrastructure - Infrastructure.jws contains the database schema information for the sample application. All
applications use the same schema, FOD. The accompanying Developer's Guide for this module is Oracle�
Fusion Middleware Fusion Developer's Guide for Oracle Application Development Framework 11g (http://download.oracle.com/docs/cd/E16162_01/web.1112/e16182/toc.htm).

MasterPriceList - MasterPriceList.jws is a sample application that integrates with Microsoft Excel to demonstrate
the use of ADF Data Integration functionality. The accompanying Developer's Guide for this module is Oracle� Fusion
Middleware Desktop Integration Developer's Guide for Oracle Application Development Framework 11g (http://download.oracle.com/docs/cd/E16162_01/web.1112/e16180/toc.htm).

StandaloneExamples - This module contains several workspaces that demonstrate various features of the ADF Framework
that are not included in the Store Front scenario. The accompanying Developer's Guide for this module is Oracle�
Fusion Middleware Fusion Developer's Guide for Oracle Application Development Framework 11g (http://download.oracle.com/docs/cd/E16162_01/web.1112/e16182/toc.htm).

StoreFrontModule - StoreFrontModule.jws is a sample web application based on Oracle ADF Business Components,
ADF Model data bindings and ADF Faces. The application follows an online shopping scenario and contains hook points
for integrating with the CompositeServices module. The accompanying Developer's Guide for this module is Oracle�
Fusion Middleware Fusion Developer's Guide for Oracle Application Development Framework 11g (http://download.oracle.com/docs/cd/E16162_01/web.1112/e16182/toc.htm).


The sample application is provided for informational purposes only.



Credits

Fusion Order Demo was created by:

Laura Akel
Duncan Mills
Lynn Munsinger
Juan Ruiz
Clemens Utschig
Kundan Vyas